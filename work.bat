@echo off

if [%MY_GIT_REPOS_DIR%] == [] (
    echo ======================================================
	echo ^> ERROR: Variable MY_GIT_REPOS_DIR not found
    echo ======================================================
    goto :EOF
)

if /I [%1] == [/h] (
    echo ======================================================
	echo ^> Usage: work [workspace_dir] [HW_DEBUG: 1 (default^) ^| 0]
    echo ======================================================
    goto :EOF
)
if [%1] == [] (
    echo ======================================================
	echo ^> Error: Must specify [workspace_dir] and [HW_DEBUG]
	echo ^> Usage: work_vs [workspace_dir] [HW_DEBUG: 1 (default^) ^| 0]
    echo ======================================================
    goto :EOF
)

if [%2] == [] (
	@echo on
	set HW_DEBUG=1
	@echo off
	GOTO :SETME
)
if %2==0 (
	@echo on
	set HW_DEBUG=0
	@echo off
)
if %2==1 (
	@echo on
	set HW_DEBUG=1
	@echo off
)

:SETME

set MY_WORKSPACE=%1
if not exist %MY_GIT_REPOS_DIR%/%MY_WORKSPACE% (
    echo ======================================================
	echo ^> Error: Workspace not found
    echo ======================================================
	goto :EOF
)

echo:
echo ======================================================
echo ^> Setting up VS Code Environment...
echo ======================================================

echo MY_WORKSPACE=%MY_WORKSPACE%

REM Creating backslash path for git repos dir and hwrootdir. Otherwise commands don't work
set MY_GIT_REPOS_DIR_BS=%MY_GIT_REPOS_DIR:/=\%
set HW_ROOTDIR_BS=%MY_GIT_REPOS_DIR_BS%\%MY_WORKSPACE%\hwdesktop
set HWDESKTOP_SETUP_DIR=%MY_GIT_REPOS_DIR_BS%\hwdesktop_setup

REM This var is used by vscode to run bat scripts from tasks.
set TASKSCRIPTS=%HWDESKTOP_SETUP_DIR%\task_scripts
echo TASKSCRIPTS=%TASKSCRIPTS%

if exist %HW_ROOTDIR_BS%\.vscode rd /s /q %HW_ROOTDIR_BS%\.vscode
mkdir  %HW_ROOTDIR_BS%\.vscode
echo *.json > %HW_ROOTDIR_BS%/.vscode/.gitignore
mklink %HW_ROOTDIR_BS%\.vscode\tasks.json          %HWDESKTOP_SETUP_DIR%\hwdesktop.vscode\tasks.json
mklink %HW_ROOTDIR_BS%\.vscode\launch.json         %HWDESKTOP_SETUP_DIR%\hwdesktop.vscode\launch.json
mklink %HW_ROOTDIR_BS%\.vscode\settings.json       %HWDESKTOP_SETUP_DIR%\hwdesktop.vscode\settings.json
mklink %HW_ROOTDIR_BS%\.vscode\cpp_properties.json %HWDESKTOP_SETUP_DIR%\hwdesktop.vscode\cpp_properties.json

echo:
call setme.bat %MY_WORKSPACE% %HW_DEBUG%
echo Running:
echo code %HW_ROOTDIR_BS%
call code %HW_ROOTDIR_BS%

:EOF