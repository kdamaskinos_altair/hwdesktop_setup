@echo off

if [%MY_GIT_REPOS_DIR%] == [] (
    echo ======================================================
	echo ^> ERROR: Variable MY_GIT_REPOS_DIR not found.
    echo ======================================================
    goto :EOF
)

if /I [%1] == [/h] (
    echo ======================================================
	echo ^> Usage: build [workspace_dir] [HW_DEBUG: 1 ^| 0]
    echo ======================================================
    goto :EOF
)

if [%1] == [] (
    echo ======================================================
	echo ^> Error: Must specify [workspace_dir] and [HW_DEBUG]
	echo ^> Usage: build [workspace_dir] [HW_DEBUG: 1 ^| 0]
    echo ======================================================
    goto :EOF
)

set MY_WORKSPACE=%1
if not exist %MY_GIT_REPOS_DIR%/%MY_WORKSPACE% (
    echo ======================================================
	echo ^> Error: Workspace not found
    echo ======================================================
	goto :EOF
)

if [%2] == [] (
    echo ======================================================
	echo ^> Error: Must specify [workspace_dir] and [HW_DEBUG]
	echo ^> Usage: build [workspace_dir] [HW_DEBUG: 1 ^| 0]
    echo ======================================================
    goto :EOF
)

call setmemdi.bat %1 %2
echo ======================================================
echo ^> Building MDI...
echo ======================================================
cd %HW_ROOTDIR%
echo make -f build-mdi.cfg
make -f build-mdi.cfg

echo:
cd %MY_GIT_REPOS_DIR%
call setme.bat %1 %2
echo ======================================================
echo ^> Opening build gui...
echo ======================================================
call %HW_ROOTDIR%\hm\HwBuildGui.bat

:EOF