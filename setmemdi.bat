@echo off

if [%MY_GIT_REPOS_DIR%] == [] (
    echo ======================================================
	echo ^> ERROR: Variable MY_GIT_REPOS_DIR not found.
    echo ======================================================
    goto :EOF
)

if /I [%1] == [/h] (
    echo ======================================================
	echo ^> Usage: setme [workspace_dir] [HW_DEBUG: 1 ^| 0]
    echo ======================================================
    goto :EOF
)

if [%1] == [] (
    echo ======================================================
	echo ^> Error: Must specify [workspace_dir] and [HW_DEBUG]
	echo ^> Usage: setme [workspace_dir] [HW_DEBUG: 1 ^| 0]
    echo ======================================================
    goto :EOF
)

set MY_WORKSPACE=%1
if not exist %MY_GIT_REPOS_DIR%/%MY_WORKSPACE% (
    echo ======================================================
	echo ^> Error: Workspace not found
    echo ======================================================
	goto :EOF
)

if [%2] == [] (
    echo ======================================================
	echo ^> Error: Must specify [workspace_dir] and [HW_DEBUG]
	echo ^> Usage: setme [workspace_dir] [HW_DEBUG: 1 ^| 0]
    echo ======================================================
    goto :EOF
)

echo ======================================================
echo ^> Setting up MDI sandbox...
echo ======================================================

set CYGWIN=noacl

echo:
set HW_DEBUG=%2
echo HW_DEBUG=%HW_DEBUG%
set HW_TARGET_ARCH=64
echo HW_TARGET_ARCH=%HW_TARGET_ARCH%

set OS_BITS=win%HW_TARGET_ARCH%
if %HW_DEBUG%==1 set OS_BITS=%OS_BITS%d

set HW_PLATFORM=%OS_BITS%
echo HW_PLATFORM=%HW_PLATFORM%

set HW_ROOTDIR=%MY_GIT_REPOS_DIR%/%MY_WORKSPACE%/mdi
echo HW_ROOTDIR=%HW_ROOTDIR%

set ALTAIR_HOME=%HW_ROOTDIR%
echo ALTAIR_HOME=%ALTAIR_HOME%

set HW_DEVEL_BLD=1
echo HW_DEVEL_BLD=%HW_DEVEL_BLD%
set HW_DEBUG_REL=1
echo HW_DEBUG_REL=%HW_DEBUG_REL%

echo:
set HW_MDI=%MY_GIT_REPOS_DIR%/%MY_WORKSPACE%/common/mdi/%HW_PLATFORM%/hwx
echo HW_MDI=%HW_MDI%
set HW_FRAMEWORK=%MY_GIT_REPOS_DIR%/%MY_WORKSPACE%/common/framework/%HW_PLATFORM%/hwx
echo HW_FRAMEWORK=%HW_FRAMEWORK%
set HW_INTERNALS=%MY_GIT_REPOS_DIR%/internal
echo HW_INTERNALS=%HW_INTERNALS%
set INTERNAL_ROOT =%HW_INTERNALS%
set HW_THIRDPARTY=%MY_GIT_REPOS_DIR%/third_party
echo HW_THIRDPARTY=%HW_THIRDPARTY%

echo:
echo ======================================================
set BVT_ROOT=%MY_GIT_REPOS_DIR%/bvt
echo BVT_ROOT=%BVT_ROOT%

echo:
echo ======================================================
set PYTHONPATH=%HW_FRAMEWORK%/bin/%OS_BITS%
set PYTHONPATH=%PYTHONPATH%;%HW_THIRDPARTY%/python/python3.8.10/%OS_BITS%/DLLs
set PYTHONPATH=%PYTHONPATH%;%ALTAIR_HOME%/../common/mdi/%OS_BIT%/hwx/bin/%OS_BITS%
set PYTHONPATH=%PYTHONPATH%;%ALTAIR_HOME%/../common/mdi/%OS_BIT%/hwx/scripts/python
set PYTHONPATH=%PYTHONPATH%;%ALTAIR_HOME%/../bvt/mdi/corepy/scripts
set PYTHONPATH=%PYTHONPATH%;%HW_MDI%/bin/%OS_BITS%
set PYTHONPATH=%PYTHONPATH%;%HW_MDI%/scripts
echo PYTHONPATH=%PYTHONPATH%

set ADDPATH=%ADDPATH%;%HW_THIRDPARTY%/python/python3.8.10/%OS_BITS%
set ADDPATH=%ADDPATH%;%HW_THIRDPARTY%/python/python3.8.10/%OS_BITS%/Scripts
set ADDPATH=%ADDPATH%;%HW_THIRDPARTY%/python/python3.8.10/%OS_BITS%/Lib/site-packages
set ADDPATH=%ADDPATH%;%HW_FRAMEWORK%/bin/%OS_BITS%
set ADDPATH=%ADDPATH%;%HW_MDI%/bin/%OS_BITS%
set PATH=%ADDPATH%;%PATH%
echo:
echo ======================================================
echo PATH Appended with:
echo %ADDPATH%

echo:
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build\vcvars64.bat"
REM call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build\vcvars64.bat" -vcvars_ver=14.28
echo:
