# HWDESKTOP SETUP

## Initial set up

1. Clone the repo into the directory where all you workspaces repos live
2. Add the repo's direcotry into `PATH` so you can have access to the scripts
     > If you do not wish to do this, you call the scripts like this: \
     > `call hwdesktop_setup\work ...`
3. Add env var `MY_GIT_REPOS_DIR` to point where you workspace repos live. **USE FORWOARD SLASHES!!!**

## Scripts

### Options

- `/h`: Prints usage message
  e.g. `work /h`
- `<workspace_dir>` → Parent folder of hwdesktop
- `<HW_DEBUG>` → Sets HW_DEBUG to 1 or 0

### Open IDE
#### VS Code
`work [workspace_dir] [HW_DEBUG: 1 (default) | 0]`

* Sets up hwdesktop sandbox for development in VS Code.  
* Creates a junction of `hwdesktop.vscode` into `<workspace_dir>/hwdesktop/.vscode`  
* Any change you make in the .vscode jsons from you workspace, will be common to all the other workspaces you work on through the `work.bat` script.

#### Visual Studio
`work_vs [workspace_dir] [HW_DEBUG: 1 (default) | 0]`

* Sets up hwdesktop sandbox for development in Visual Studio

### Build  hwdesktop ONLY : 
`build [workspace_dir] [HW_DEBUG: 1 | 0]`

* Sets up hwdesktop sandbox and invokes build gui.

### Build  MDI + hwdesktop: 
You must clone https://gitlab.com/altairengineering/modvis/devops/devteams/mdi/customdevtools into `MY_GIT_REPOS_DIR`

`buildmdi [workspace_dir] [HW_DEBUG: 1 | 0]`

* Sets up mdi sandbox and builds mdi repo
* After mdi repo build is done, sets up hwdesktop sandbox and invokes build gui.


### Sandbox Scripts
You don't really need to call these directly. They get called by the build and work scripts.

`setme [workspace_dir] [HW_DEBUG: 1 | 0]`

* Sets up hwdesktop sandbox.

`setmemdi [workspace_dir] [HW_DEBUG: 1 | 0]`

* Sets mdi sandbox.
* Sets up hwdesktop sandbox.

## VS Code Tasks

In `\task_scripts` you can place any bat file you would like to run though VS Code Tasks.  

Then in `hwdesktop.vscode\tasks.json` under the `tasks[]` attribute add a task like this:

```json
{
    "label": "build_pythonrecorder",
    "command": "${env:TASKSCRIPTS}/build_pythonrecorder.bat",
    "group": "build"
}
```

## VS Code Tips

### Launch Tasks

In `\hwdesktop.vscode\launch.json`  you can specify launch tasks for what ever you need.  

A useful feature is compound tasks like runhwx and then Attach.

```json
    "compounds": [
        {
            "name": "runhwx & Attach",
            "configurations": ["runhwx", "Attach - cpp"],
            "presentation": {
                "hidden": false,
                "group": "runhwx"
            }
        }
    ]
```

The Attach launch task will show a processes list.\
There you need to search for hwx and then refresh the list to attach. \
To refresh the list you need to click the circular arrow to the top-right of the pop-up list.

### Defines

In `\hwdesktop.vscode\cpp_properties.json`  you can specify any defines you use like enviromental variables.

```json
            "defines": [
                "_DEBUG",
                "UNICODE",
                "_UNICODE",
                "OS_WIN",
                "${env:HW_DEBUG}"
            ],
```