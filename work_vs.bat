@echo off

if [%MY_GIT_REPOS_DIR%] == [] (
    echo ======================================================
	echo ^> ERROR: Variable MY_GIT_REPOS_DIR not found.
    echo ======================================================
    goto :EOF
)

if /I [%1] == [/h] (
    echo ======================================================
	echo ^> Usage: work_vs [workspace_dir] [HW_DEBUG: 1 (default^) ^| 0]
    echo ======================================================
    goto :EOF
)
if [%1] == [] (
    echo ======================================================
	echo ^> Error: Must specify [workspace_dir] and [HW_DEBUG]
	echo ^> Usage: work_vs [workspace_dir] [HW_DEBUG: 1 (default^) ^| 0]
    echo ======================================================
    goto :EOF
)

if [%2] == [] (
	@echo on
	set HW_DEBUG=1
	@echo off
	GOTO :SETME
)
if %2==0 (
	@echo on
	set HW_DEBUG=0
	@echo off
)
if %2==1 (
	@echo on
	set HW_DEBUG=1
	@echo off
)

:SETME

set MY_WORKSPACE=%1
if not exist %MY_GIT_REPOS_DIR%/%MY_WORKSPACE% (
    echo ======================================================
	echo ^> Error: Workspace not found
    echo ======================================================
	goto :EOF
)

echo:
echo ======================================================
echo ^> Setting up Visual Studio Environment...
echo ======================================================

echo MY_WORKSPACE=%MY_WORKSPACE%

echo:
call setme.bat %MY_WORKSPACE% %HW_DEBUG%
echo Running:
echo devenv %MY_GIT_REPOS_DIR%/%MY_WORKSPACE%/hwdesktop
call devenv %MY_GIT_REPOS_DIR%/%MY_WORKSPACE%/hwdesktop

:EOF