@echo off

if [%MY_GIT_REPOS_DIR%] == [] (
    echo ======================================================
	echo ^> ERROR: Variable MY_GIT_REPOS_DIR not found.
    echo ======================================================
    goto :EOF
)

if /I [%1] == [/h] (
    echo ======================================================
	echo ^> Usage: setme [workspace_dir] [HW_DEBUG: 1 ^| 0]
    echo ======================================================
    goto :EOF
)

if [%1] == [] (
    echo ======================================================
	echo ^> Error: Must specify [workspace_dir] and [HW_DEBUG]
	echo ^> Usage: setme [workspace_dir] [HW_DEBUG: 1 ^| 0]
    echo ======================================================
    goto :EOF
)

set MY_WORKSPACE=%1
if not exist %MY_GIT_REPOS_DIR%/%MY_WORKSPACE% (
    echo ======================================================
	echo ^> Error: Workspace not found
    echo ======================================================
	goto :EOF
)

if [%2] == [] (
    echo ======================================================
	echo ^> Error: Must specify [workspace_dir] and [HW_DEBUG]
	echo ^> Usage: setme [workspace_dir] [HW_DEBUG: 1 ^| 0]
    echo ======================================================
    goto :EOF
)

echo ======================================================
echo ^> Setting up HM sandbox...
echo ======================================================

set ALTAIR_HOME=%MY_GIT_REPOS_DIR%/%MY_WORKSPACE%
echo ALTAIR_HOME=%ALTAIR_HOME%

REM set to 1 for username licensing issue when running BVTs
set QA_MODE=1
echo:
echo QA_MODE=%QA_MODE%  # Because of issues with licensing when username is long

echo:
set HW_DEBUG=%2
echo HW_DEBUG=%HW_DEBUG%
set HW_TARGET_ARCH=64
echo HW_TARGET_ARCH=%HW_TARGET_ARCH%

set OS_BITS=win%HW_TARGET_ARCH%
if %HW_DEBUG%==1 set OS_BITS=%OS_BITS%d

set HW_PLATFORM=%OS_BITS%
echo HW_PLATFORM=%HW_PLATFORM%

set HW_ROOTDIR=%MY_GIT_REPOS_DIR%/%MY_WORKSPACE%/hwdesktop
echo HW_ROOTDIR=%HW_ROOTDIR%
set HW_UNITY_ROOTDIR=%HW_ROOTDIR%/unity
echo HW_UNITY_ROOTDIR=%HW_UNITY_ROOTDIR%

set HW_DEVEL_BLD=1
echo HW_DEVEL_BLD=%HW_DEVEL_BLD%
set HW_DEBUG_REL=1
echo HW_DEBUG_REL=%HW_DEBUG_REL%
set HW_OPT=0
echo HW_OPT=%HW_OPT%

echo:
set HW_MDI=%MY_GIT_REPOS_DIR%/%MY_WORKSPACE%/common/mdi/%HW_PLATFORM%/hwx
echo HW_MDI=%HW_MDI%
set HW_FRAMEWORK=%MY_GIT_REPOS_DIR%/%MY_WORKSPACE%/common/framework/%HW_PLATFORM%/hwx
echo HW_FRAMEWORK=%HW_FRAMEWORK%
set HW_INTERNALS=%MY_GIT_REPOS_DIR%/internal
echo HW_INTERNALS=%HW_INTERNALS%
set HW_THIRDPARTY=%MY_GIT_REPOS_DIR%/third_party
echo HW_THIRDPARTY=%HW_THIRDPARTY%

echo:
echo ======================================================
echo ^> Setting up BVTs Environment...
echo ======================================================
REM TESTS
REM SWICH TO BACKSLASH FOR THIS -> Invalid switch - "Software".
REM mklink /j  %MY_GIT_REPOS_DIR%\%MY_WORKSPACE%\bvt %MY_GIT_REPOS_DIR%\bvt
REM mklink /j  %MY_GIT_REPOS_DIR%\%MY_WORKSPACE%\testtools %MY_GIT_REPOS_DIR%\testtools

set BVT_ROOT=%MY_GIT_REPOS_DIR%/bvt
echo BVT_ROOT=%BVT_ROOT%
set BUILDTOOLS=%MY_GIT_REPOS_DIR%/buildtools
echo BUILDTOOLS=%BUILDTOOLS%
set ACT_SCRIPTS=%MY_GIT_REPOS_DIR%/act_scripts
echo ACT_SCRIPTS=%ACT_SCRIPTS%
set MODULES_TEST_FILES_ROOT=%ACT_SCRIPTS%/FilesForModulesTestScripts
echo MODULES_TEST_FILES_ROOT=%MODULES_TEST_FILES_ROOT%
set HM_PRIVATE_SUPRESS_INVALID_HIERARCHY_MESSAGE=1
echo HM_PRIVATE_SUPRESS_INVALID_HIERARCHY_MESSAGE=%HM_PRIVATE_SUPRESS_INVALID_HIERARCHY_MESSAGE%

echo:
echo ======================================================
echo ^> Setting Environmental Variables...
echo ======================================================
set HW_ENABLE_PYTHON=1
echo HW_ENABLE_PYTHON=%HW_ENABLE_PYTHON%
set HW_DEV_STUDIOS=1
echo HW_DEV_STUDIOS=%HW_DEV_STUDIOS%
set HW_STUDIO_TOOLS=1
echo HW_STUDIO_TOOLS=%HW_STUDIO_TOOLS%
set HM_EXPOSE_HIDDEN=1
echo HM_EXPOSE_HIDDEN=%HM_EXPOSE_HIDDEN%
set HM_MULTIPLE_MODEL_SUPPORT=1
echo HM_MULTIPLE_MODEL_SUPPORT=%HM_MULTIPLE_MODEL_SUPPORT%
set HM_HMSERVICES_POST_LOGS=1
echo HM_HMSERVICES_POST_LOGS=%HM_HMSERVICES_POST_LOGS%
set HM_PYTHON_RECORDER=1
echo HM_PYTHON_RECORDER=%HM_PYTHON_RECORDER%

echo:
echo ======================================================
set PYTHONPATH=^
%HW_ROOTDIR%/hw/bin/%HW_PLATFORM%;^
%HW_ROOTDIR%/hw/python;^
%HW_ROOTDIR%/hw/python/hpl;^
%HW_FRAMEWORK%/bin/%HW_PLATFORM%;^
%HW_THIRDPARTY%/python/python3.8.10/%HW_PLATFORM%/DLLs;^
%BVT_ROOT%/mdi/mdiprofile/scripts;^
%BVT_ROOT%/mdi/corepy/scripts;
echo PYTHONPATH=%PYTHONPATH%

set PYTHONBIN=%HW_THIRDPARTY%/python/python3.8.10/%HW_PLATFORM%/python
if %HW_DEBUG%==1 set PYTHONBIN=%PYTHONBIN%_d
set PYTHONBIN=%PYTHONBIN%.exe
echo:
echo PYTHONBIN=%PYTHONBIN%

set PATHAPPEND=^
%HW_ROOTDIR%/hm/bin/%HW_PLATFORM%;^
%HW_ROOTDIR%/hw/bin/%HW_PLATFORM%;^
%HW_ROOTDIR%/hw/python/hpl/vtkmodules;^
%HW_UNITY_ROOTDIR%/bin/%HW_PLATFORM%;^
%HW_FRAMEWORK%/bin/%HW_PLATFORM%;^
%HW_MDI%/bin/%HW_PLATFORM%;^
%HW_INTERNALS%/netlib/bin/%HW_PLATFORM%;^
%HW_THIRDPARTY%/python/python3.8.10/%HW_PLATFORM%;^
%HW_THIRDPARTY%/python/python3.8.10/%HW_PLATFORM%/Scripts;^
%HW_THIRDPARTY%/python/python3.8.10/%HW_PLATFORM%/Lib/site-packages;
set PATH==%PATH%;%PATHAPPEND%
echo:
echo ======================================================
echo PATH Appended with:
echo %PATHAPPEND%

echo:
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build\vcvars64.bat"
REM call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build\vcvars64.bat" -vcvars_ver=14.28
echo:
